#include "DHT.h"

// The NodeMCU has some weird pin numbering, but are defined:
// D0   = 16;
// D1   = 5;
// D2   = 4;
// D3   = 0;
// D4   = 2;
// D5   = 14;
// D6   = 12;
// D7   = 13;
// D8   = 15;
// D9   = 3;
// D10  = 1;

DHT dht(D1, DHT22);

void setup() {
  Serial.begin(9600);
  dht.begin();
  delay(10);
}

void loop() {
  readSensor();
  delay(1000);
}

void readSensor(){
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(h) || isnan(t)) {
    Serial.println("Reading sensor failed");
    delay(1000);
    return;
  }
  Serial.print(h); Serial.print("% ");
  Serial.print(t); Serial.println(" ºC");
}

