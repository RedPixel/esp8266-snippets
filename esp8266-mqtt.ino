#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#define WIFISSID "<YOUR SSID>"
#define WIFIPASS "<YOUR PASSWORD>"
#define MQTTSERV "<MQTT SERVER IP/HOST>"
#define MQTTPORT 1883
#define MQTTUSER "<YOUR MQTT USER>"
#define MQTTPASS "<YOUR MQTT PASS>"
#define TOPIC "test"

WiFiClient espClient;
PubSubClient mqttclient;
int i = 0;
void setup() {
  Serial.begin(9600);
  
  delay(10);
  connectWifi();
  mqttclient.setClient(espClient);
  mqttclient.setServer(MQTTSERV, MQTTPORT);
}

void connect_mqtt() {
  while (!mqttclient.connected()) {
    Serial.print("Connecting to MQTT on ");
    Serial.print(MQTTUSER);
    Serial.print(':');
    Serial.print(MQTTSERV);
    Serial.print(':');
    Serial.println(MQTTPORT);
    
    if (mqttclient.connect("ESP8266Client", MQTTUSER, MQTTPASS)) {
      Serial.println("MQTT connected");
    } else {
      Serial.print("MQTT connection failed: ");
      Serial.print(mqttclient.state());
      delay(5000);
    }
  }
}

void connectWifi(){
  Serial.print("Connecting to "); Serial.println(WIFISSID);
  WiFi.begin(WIFISSID, WIFIPASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(WiFi.status()); Serial.print(' ');
  }
  Serial.println(' ');
  Serial.print("Connected to "); Serial.println(WIFISSID);
  Serial.print("IP address:\t"); Serial.println(WiFi.localIP());
}

void loop() {
  if(WiFi.status() != WL_CONNECTED){
    connectWifi();
  }
  if (!mqttclient.connected()) {
    connect_mqtt();
  }
  String message1 = "test message ";
  String temp = message1 + i++;
  mqttclient.publish(TOPIC, String(temp).c_str());
  
  delay(1000);
}