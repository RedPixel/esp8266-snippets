#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "DHT.h"

// edit the following defines as needed

#define WIFISSID "<Your Wifi SSID>"
#define WIFIPASS "<Your Wifi Password>"
#define MQTTSERV "<Your MQTT Server>"
#define MQTTPORT 1883
#define MQTTID "HuiskamerSensor"
#define MQTTUSER "<MQTT User>"
#define MQTTPASS "<MQTT Password>"
#define TEMPTOPIC "sensors/livingroom/temperature"
#define HUMIDTOPIC "sensors/livingroom/humidity"

WiFiClient espClient;
PubSubClient mqttclient;
int i = 0;

DHT dht(D2, DHT22); // my DHT22 is connected to D2

void setup() {
  Serial.begin(9600);

  while (!Serial) {}

  delay(100);

  mqttclient.setClient(espClient);
  mqttclient.setServer(MQTTSERV, MQTTPORT);

  if (WiFi.status() != WL_CONNECTED) {
    connectWifi();
  }


  float h = dht.readHumidity();
  float t = dht.readTemperature();
  String hmsg = String(h) + String(" % ");
  String tmsg = String(t) + String(" ºC");

  Serial.print("Sending ");
  Serial.print(hmsg);
  Serial.print(" to ");
  Serial.println(HUMIDTOPIC);

  Serial.print("Sending ");
  Serial.print(tmsg);
  Serial.print(" to ");
  Serial.println(TEMPTOPIC);

  if (!mqttclient.connected()) {
    connect_mqtt();
  }
  mqttclient.publish(HUMIDTOPIC, hmsg.c_str(), true);
  mqttclient.publish(TEMPTOPIC, tmsg.c_str(), true);
  delay(500);
  mqttclient.disconnect();
  Serial.println("Finished sending to MQTT, going to sleep");
  delay(500); // Apparently needed to allow receiving messages on MQTT Server.

  ESP.deepSleep(60e6); // 60e6 is 60 seconds
}

void connect_mqtt() {
  while (!mqttclient.connected()) {
    Serial.print("Connecting to MQTT on ");
    Serial.print(MQTTUSER);
    Serial.print('@');
    Serial.print(MQTTSERV);
    Serial.print(':');
    Serial.println(MQTTPORT);

    if (mqttclient.connect(MQTTID, MQTTUSER, MQTTPASS)) {
      Serial.println("MQTT connected");
    } else {
      Serial.print("MQTT connection failed: ");
      Serial.println(mqttclient.state());
      ESP.deepSleep(60e6); // 10e6 is 10 seconds
    }
  }
}

void connectWifi() {
  Serial.print("Connecting to "); Serial.println(WIFISSID);
  WiFi.persistent(false);
  WiFi.mode(WIFI_OFF);   // this is a temporary line, to be removed after SDK update to 1.5.4
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFISSID, WIFIPASS);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(WiFi.status());
    Serial.print(' ');
  }
  Serial.println(' ');
  Serial.print("Connected to "); Serial.println(WIFISSID);
  Serial.print("IP address:\t"); Serial.println(WiFi.localIP());
}

void loop() {
  // not used due to deep sleep
}