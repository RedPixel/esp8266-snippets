#include <ESP8266WiFi.h>
#define WIFISSID "YOUR_SSID_HERE"
#define WIFIPW "YOUR_PASSWORD_HERE"

void setup() {
  Serial.begin(9600);
  delay(1000);
  connectWifi();
}

//typedef enum {
//    WL_NO_SHIELD        = 255,   // for compatibility with WiFi Shield library
//    WL_IDLE_STATUS      = 0,
//    WL_NO_SSID_AVAIL    = 1,
//    WL_SCAN_COMPLETED   = 2,
//    WL_CONNECTED        = 3,
//    WL_CONNECT_FAILED   = 4,
//    WL_CONNECTION_LOST  = 5,
//    WL_DISCONNECTED     = 6
//} wl_status_t;

void connectWifi(){
  Serial.print("Connecting to "); Serial.println(WIFISSID);
  WiFi.begin(WIFISSID, WIFIPW);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(WiFi.status());Serial.print(' ');
  }
  Serial.println(' ');
  Serial.print("Connected to "); Serial.println(WIFISSID);
  Serial.print("IP address:\t"); Serial.println(WiFi.localIP());
}

void loop() {

}