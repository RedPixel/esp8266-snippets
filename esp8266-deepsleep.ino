void setup() {
  Serial.begin(9600);
  while (!Serial) {}; // wait for serial to come up
  Serial.println("Hello world!");
  ESP.deepSleep(10e6); // sleep for 10 seconds
  // Note that deepSleep is limited to 4.294.967.295 µs (72 minutes)
}

void loop() {
  // with deep sleep you can not use loop!
}